#!/bin/sh

KEYRING=/usr/share/keyrings/debian-keyring.gpg

if ! gpgv --keyring $KEYRING keys.sha256.asc keys.sha256; then
	echo "\033[0;31m"
	echo "GPG SIGNATURE VERIFICATION FAILED!"
	echo "\033[0m"
	exit 1
else
	if ! sha256sum -c keys.sha256; then
		echo "\033[0;31m"
		echo "SHA256 VERIFICATION FAILED!"
	        echo "\033[0m"
		exit 1
	else
		echo "\033[0;32m"
		echo "Verification successful!"
		echo "\033[0m"
		exit 0
	fi
fi
