signify-openbsd-keys (2024.2) unstable; urgency=medium

  * Keys for openbsd 77

 -- Tomasz Buchert <tomasz@debian.org>  Sat, 17 Aug 2024 00:14:47 +0200

signify-openbsd-keys (2024.1) unstable; urgency=medium

  * Keys for openbsd 76

 -- Tomasz Buchert <tomasz@debian.org>  Thu, 22 Feb 2024 21:52:43 +0100

signify-openbsd-keys (2023.2) unstable; urgency=medium

  * Keys for openbsd 75

 -- Tomasz Buchert <tomasz@debian.org>  Sun, 12 Nov 2023 16:44:53 +0100

signify-openbsd-keys (2023.1) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + signify-openbsd-keys: Add Multi-Arch: foreign.

  [ Tomasz Buchert ]
  * Keys for openbsd 74
  * Update standards version to 4.6.2, no changes needed.

 -- Tomasz Buchert <tomasz@debian.org>  Sat, 11 Mar 2023 23:50:04 +0100

signify-openbsd-keys (2022.2) unstable; urgency=medium

  * Add keys for openbsd 73

 -- Tomasz Buchert <tomasz@debian.org>  Sat, 06 Aug 2022 10:47:58 +0200

signify-openbsd-keys (2022.1) unstable; urgency=medium

  * Add keys for openbsd 72

 -- Tomasz Buchert <tomasz@debian.org>  Sat, 30 Apr 2022 15:42:57 +0200

signify-openbsd-keys (2021.2) unstable; urgency=medium

  * Added keys for openbsd 71

 -- Tomasz Buchert <tomasz@debian.org>  Wed, 29 Sep 2021 21:37:52 +0200

signify-openbsd-keys (2021.1) unstable; urgency=medium

  * Added keys for openbsd 70
  * bump std-ver to 4.5.1, no changes needed

 -- Tomasz Buchert <tomasz@debian.org>  Tue, 02 Mar 2021 09:32:28 +0100

signify-openbsd-keys (2020.2) unstable; urgency=medium

  * Added keys for openbsd 69
  * debhelper 13

 -- Tomasz Buchert <tomasz@debian.org>  Sun, 27 Sep 2020 14:16:05 +0200

signify-openbsd-keys (2020.1) unstable; urgency=medium

  * Added keys for openbsd 68
  * Update std-ver to 4.5.0

 -- Tomasz Buchert <tomasz@debian.org>  Sun, 01 Mar 2020 13:38:27 +0100

signify-openbsd-keys (2019.2) unstable; urgency=medium

  * Release with all openbsd 67 keys
  * Update std-ver to 4.4.0 (no changes needed)

 -- Tomasz Buchert <tomasz@debian.org>  Thu, 22 Aug 2019 13:33:16 +0200

signify-openbsd-keys (2019.1) unstable; urgency=medium

  * Release with openbsd 67 pkg key
  * Remove entry for 2018.5 which was never formally released

 -- Tomasz Buchert <tomasz@debian.org>  Tue, 06 Aug 2019 16:43:00 +0200

signify-openbsd-keys (2018.6) unstable; urgency=medium

  * Release with openbsd-67-base.pub (Closes: #933610)

 -- Tomasz Buchert <tomasz@debian.org>  Sun, 04 Aug 2019 13:18:34 +0200

signify-openbsd-keys (2018.4) unstable; urgency=medium

  * refresh packaging (debhelper 12, std-ver 4.3.0)

 -- Tomasz Buchert <tomasz@debian.org>  Thu, 24 Jan 2019 00:01:10 +0100

signify-openbsd-keys (2018.3) unstable; urgency=medium

  * Release with openbsd-65 keys

 -- Tomasz Buchert <tomasz@debian.org>  Thu, 22 Nov 2018 09:33:39 +0100

signify-openbsd-keys (2018.2) unstable; urgency=medium

  * Release with openbsd-64 keys

 -- Tomasz Buchert <tomasz@debian.org>  Wed, 15 Aug 2018 09:52:40 +0200

signify-openbsd-keys (2018.1) unstable; urgency=medium

  * Added new keys
  * Update compat to 11
  * Update std-ver to 4.1.3

 -- Tomasz Buchert <tomasz@debian.org>  Tue, 02 Jan 2018 14:41:28 +0100

signify-openbsd-keys (2017.1) unstable; urgency=medium

  * d/control: make myself maintainer and refresh packaging (Closes: #855641)
  * Added an easy way to refresh keys
  * Updated the list of OpenBSD public keys

 -- Tomasz Buchert <tomasz@debian.org>  Wed, 05 Jul 2017 08:12:56 +0200

signify-openbsd-keys (2015.1) unstable; urgency=low

  * Initial Release.

 -- Riley Baird <BM-2cVqnDuYbAU5do2DfJTrN7ZbAJ246S4XiX@bitmessage.ch>  Mon, 30 Nov 2015 08:21:32 +1100
